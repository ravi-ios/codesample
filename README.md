# MicroBlog-iOS client application

##### System requirements

- Xcode: v11.5 later
- OS: iOS 13.0 later
- Supported devices: iPhone
- Supported orientations: Portrait

Application has 3 basic features

- List of authors
- Author details and his collection of posts
- Post details and its comments list

The application was architected with the best practices that are inspired by WWDC, Apple's documentation, and various knowledge-sharing conferences.

The main building blocks are

- Network layer
- Cache layer
- User interface

Responsibilities are divided among them and they are self-explanatory.

Note: Some time "ReverseGeocodeLocation" won't work in the iOS simulator, choosing a different location in Xcode may give better results, and it won't be a problem on a physical device.

The application is scalable and improved very well due to the fact of its architectural design foundation.
