//
//  Date.swift
//  MicroBlog
//
//  Created by Ravikumar Chintakayala on 28/06/2020.
//  Copyright © 2020 Personal. All rights reserved.
//

import Foundation

extension Date {

    func string(format: String) -> String {
        let formatter = DateFormatter()
        formatter.locale = Locale.autoupdatingCurrent
        formatter.calendar = Calendar.autoupdatingCurrent
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
    
}


