//
//  UIImageView.swift
//  MicroBlog
//
//  Created by Ravikumar Chintakayala on 28/06/2020.
//  Copyright © 2020 Personal. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {
    
    func imageWithPath(_ path: String) {
        APIClient.shared.download(URL(string: path)) { data, error in
            guard let data = data else { return }
            
            self.image = UIImage(data: data)
        }
    }
    
}
