//
//  Author.swift
//  MicroBlog
//
//  Created by Ravikumar Chintakayala on 28/06/2020.
//  Copyright © 2020 Personal. All rights reserved.
//

import Foundation

struct Author: Decodable {
    
    var address: Address
    
    let avatarUrl:String
    
    let email: String
    
    let id: Int
    
    let name: String
    
    let userName: String
}
