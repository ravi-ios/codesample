//
//  Address.swift
//  MicroBlog
//
//  Created by Ravikumar Chintakayala on 28/06/2020.
//  Copyright © 2020 Personal. All rights reserved.
//

import Foundation
import CoreLocation

struct Address: Decodable {
    
    let latitude: String
    
    let longitude: String
        
    lazy var location: CLLocation = {
        return CLLocation(latitude: (latitude as NSString).doubleValue, longitude: (longitude as NSString).doubleValue)
    }()
    
}
