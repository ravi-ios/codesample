//
//  Comments.swift
//  MicroBlog
//
//  Created by Ravikumar Chintakayala on 28/06/2020.
//  Copyright © 2020 Personal. All rights reserved.
//

import Foundation

struct Comment: Decodable {
    
    let avatarUrl: String
    
    let body:String
    
    let date: String
    
    let email:String
    
    let id: Int
    
    let postId: Int
    
    let userName: String
    
    lazy var commentedDate : String? = {
        let text = date.getDate()?.string(format: "MMM dd, YYYY")
        return text != nil ? "Commented on \(text!)" : ""
    } ()
    
}
