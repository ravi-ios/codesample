//
//  CommentsTableViewController.swift
//  MicroBlog
//
//  Created by Ravikumar Chintakayala on 28/06/2020.
//  Copyright © 2020 Personal. All rights reserved.
//

import UIKit

class CommentsTableViewController: BaseTableViewController {
    
    // MARK: - Private properties

    private var comments = [Comment]()
    private var headerView: CommentsHeaderView!
    
    // MARK: - Public properties
    
    public var post : Post!
    
    // MARK: - ViewController life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        headerView = CommentsHeaderView.instantiate(post)
        
        getComments(pageNumber: 1, fetchType: .loading)
    }
    
    // MARK: - Private functions
    
    private func getComments(pageNumber: Int, fetchType: DataFetchType = .loading) {
        showLoadingAnimation(fetchType)
        ComentsService.shared.getComments(postId: post.id, pageNumber: pageNumber) { [weak self] result, error in
            guard let self = self else { return }
            
            defer { self.stopLoadingAnimation(fetchType) }
            
            guard error == nil else {
                NavigationService.shared.showAlert(message: error?.localizedDescription ?? "Sorry, some thing went wrong")
                return
            }
            
            guard let result = result else { return }
            
            self.comments.append(contentsOf: result)
            self.pageNumber += 1
            self.updateTableView(totalDataCount: self.comments.count, newDataCount: result.count)
        }
    }
    
}

// MARK: - Table view data source

extension CommentsTableViewController {
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "\(CommentTableViewCell.self)") as! CommentTableViewCell
        cell.comment = comments[indexPath.row]
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comments.count
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return headerView
    }
    
}

// MARK: - Table view delegate

extension CommentsTableViewController {
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = 0
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && comments.count > 0 {
            getComments(pageNumber: pageNumber, fetchType: .pagination)
        }
    }
    
}
