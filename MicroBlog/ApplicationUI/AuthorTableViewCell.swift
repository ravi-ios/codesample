//
//  AuthorTableViewCell.swift
//  MicroBlog
//
//  Created by Ravikumar Chintakayala on 28/06/2020.
//  Copyright © 2020 Personal. All rights reserved.
//

import UIKit

class AuthorTableViewCell: UITableViewCell {
    
    // MARK: - TableViewCell life cycle
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        avatarImageView.image = UIImage(named: "Avatar")
    }
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    // MARK: - Internal members
    
    var author: Author! {
        didSet {
            avatarImageView.imageWithPath(author.avatarUrl)
            nameLabel.text = author.name
            LocationService.shared.getAddress(author.address.location) { [weak self] address in
                guard let self = self else { return }
                self.addressLabel.text = address?.rangeOfCharacter(from: .letters) != nil ? address : ""
            }
        }
    }
    
}
