//
//  CommentsHeaderView.swift
//  MicroBlog
//
//  Created by Ravikumar Chintakayala on 28/06/2020.
//  Copyright © 2020 Personal. All rights reserved.
//

import UIKit

class CommentsHeaderView: UIView {
    
    // MARK: - Initialization & Memory Management

    class func instantiate(_ post: Post) -> CommentsHeaderView {
        let view = UINib(nibName: "\(CommentsHeaderView.self)", bundle: nil).instantiate(withOwner: nil, options: nil).first as! CommentsHeaderView
        view.post = post
        return view
    }
    
    // MARK: - IBOutlets

    @IBOutlet weak var postImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var postedDateLabel: UILabel!
    
    // MARK: - Internal members

    private var post: Post! {
        didSet {
            postImageView.imageWithPath(post.imageUrl)
            titleLabel.text = post.title
            descriptionLabel.text = post.body
            postedDateLabel.text = post.publishedDate
        }
    }

}
