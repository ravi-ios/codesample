//
//  AuthorHeaderView.swift
//  MicroBlog
//
//  Created by Ravikumar Chintakayala on 28/06/2020.
//  Copyright © 2020 Personal. All rights reserved.
//

import UIKit

class AuthorHeaderView: UIView {
    
    // MARK: - Initialization & Memory Management

    class func instantiate(_ author: Author) -> AuthorHeaderView {
        let view = UINib(nibName: "\(AuthorHeaderView.self)", bundle: nil).instantiate(withOwner: nil, options: nil).first as! AuthorHeaderView
        view.author = author
        return view
    }
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var authorImageView: UIImageView!
    @IBOutlet weak var authorNameLabel: UILabel!
    @IBOutlet weak var authorUsernameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    // MARK: - Private members
        
    private var author: Author! {
        didSet {
            authorImageView.imageWithPath(author.avatarUrl)
            authorUsernameLabel.text = author.userName
            authorNameLabel.text = author.name
            LocationService.shared.getAddress(author.address.location) { [weak self] address in
                guard let self = self else { return }
                self.addressLabel.text = address?.rangeOfCharacter(from: .letters) != nil ? address : ""
            }
        }
    }

}
