//
//  Endpoint.swift
//  MicroBlog
//
//  Created by Ravikumar Chintakayala on 28/06/2020.
//  Copyright © 2020 Personal. All rights reserved.
//

import Foundation

struct Endpoint {
    let path: String
    let queryItems: [URLQueryItem]
}

extension Endpoint {
    var url: URL? {
        var components = URLComponents()
        components.scheme = "https"
        components.host = "sym-json-server.herokuapp.com"
        components.path = path
        components.queryItems = queryItems

        return components.url
    }
}

extension Endpoint {
    
    static func authors(pageNumber: Int, pageLimit: Int) -> Endpoint {
        return Endpoint(
            path: "/authors",
            queryItems: [
                URLQueryItem(name: "_page", value: "\(pageNumber)"),
                URLQueryItem(name: "_limit", value: "\(pageLimit)")
            ]
        )
    }
    
    static func comments(postId: Int, pageNumber: Int, pageLimit: Int, sorting field: String, sortBy order: Sorting) -> Endpoint {
        return Endpoint(
            path: "/comments",
            queryItems: [
                URLQueryItem(name: "postId", value: "\(postId)"),
                URLQueryItem(name: "_page", value: "\(pageNumber)"),
                URLQueryItem(name: "_limit", value: "\(pageLimit)"),
                URLQueryItem(name: "_sort", value: "\(field)"),
                URLQueryItem(name: "_order", value: "\(order.rawValue)")
            ]
        )
    }
    
    static func posts(authorId: Int, pageNumber: Int, pageLimit: Int) -> Endpoint {
        return Endpoint(
            path: "/posts",
            queryItems: [
                URLQueryItem(name: "authorId", value: "\(authorId)"),
                URLQueryItem(name: "_page", value: "\(pageNumber)"),
                URLQueryItem(name: "_limit", value: "\(pageLimit)")
            ]
        )
    }
    
}

enum Sorting: String {
    case ascending = "asc"
    case descending = "dsc"
}
