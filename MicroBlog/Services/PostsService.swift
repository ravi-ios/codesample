//
//  PostsService.swift
//  MicroBlog
//
//  Created by Ravikumar Chintakayala on 28/06/2020.
//  Copyright © 2020 Personal. All rights reserved.
//

import Foundation

struct PostsService {
    
    let apiClient: APIClient
    
    static let shared = PostsService()
    
    init(apiClient: APIClient = .shared ) {
        self.apiClient = apiClient
    }
    
    func getPosts(autherId: Int, pageNumber: Int = 0, pageLimit: Int = 20, completionHandler: @escaping ([Post]?, Error?) -> Void) {
        let url = Endpoint.posts(authorId: autherId, pageNumber: pageNumber, pageLimit: pageLimit).url
        apiClient.get(url, completionHandler: completionHandler)
    }
    
}
